package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.CrudDataBase;
import iaf.ofek.hadracha.base_course.web_server.Data.InMemoryMapDataBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/ejectedPilotRescue")
public class EjectedPilotRestController {
    private EjectionsImporter ejectionsImporter;


    public EjectedPilotRestController( @Autowired EjectionsImporter ejectionsImporter) {
        this.ejectionsImporter = ejectionsImporter;
    }

    @GetMapping("/infos")
    public List<EjectedPilotInfo> getEjectedPilots(){
        return ejectionsImporter.dataBase.getAllOfType(EjectedPilotInfo.class);
    }

    @GetMapping("/takeResponsibility")
    public void getEjectedPilotById(HttpServletRequest request, @RequestParam int ejectionId){
        EjectedPilotInfo ejectedPilotInfo = ejectionsImporter.dataBase.getByID(ejectionId, EjectedPilotInfo.class);
        ejectedPilotInfo.rescuedBy = getClientIdFromCookies(request.getCookies());
        ejectionsImporter.dataBase.update(ejectedPilotInfo);
    }

    private String getClientIdFromCookies(Cookie[] cookies) {
        String clientId = null;
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("client-id")) {
                clientId = cookie.getValue();
            }
        }

        return clientId;
    }
}
