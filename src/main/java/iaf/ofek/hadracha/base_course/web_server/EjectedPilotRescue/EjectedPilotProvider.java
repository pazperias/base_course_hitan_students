package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface EjectedPilotProvider {
    List<EjectedPilotInfo> getAllEjectedPilots();
//    EjectedPilotInfo getEjectedPilotById(HttpServletRequest request, int id);
}
