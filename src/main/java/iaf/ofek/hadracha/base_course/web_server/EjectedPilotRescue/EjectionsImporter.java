package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.Coordinates;
import iaf.ofek.hadracha.base_course.web_server.Data.CrudDataBase;
import iaf.ofek.hadracha.base_course.web_server.Data.Entity;
import iaf.ofek.hadracha.base_course.web_server.Utilities.ListOperations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import java.util.List;
import java.util.concurrent.Executors;
import org.jetbrains.annotations.Nullable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class EjectionsImporter implements EjectedPilotProvider {

    @Value("${ejections.server.url}")
    public String EJECTION_SERVER_URL;
    @Value("${ejections.namespace}")
    public String NAMESPACE;


    private final RestTemplate restTemplate;
    public final CrudDataBase dataBase;
    private final ListOperations listOperations;
    private static final Double SHIFT_NORTH = 1.7;

    public EjectionsImporter(RestTemplateBuilder restTemplateBuilder, CrudDataBase dataBase, ListOperations listOperations) {
        restTemplate = restTemplateBuilder.build();
        this.dataBase = dataBase;
        this.listOperations = listOperations;
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(this::updateEjections, 1, 1, TimeUnit.SECONDS);

    }

    @Override
    public List<EjectedPilotInfo> getAllEjectedPilots() {
        try {
            List<EjectedPilotInfo> ejectionsFromServer;
            ResponseEntity<List<EjectedPilotInfo>> responseEntity = restTemplate.exchange(
                    EJECTION_SERVER_URL + "/ejections?name=" + NAMESPACE, HttpMethod.GET,
                    null, new ParameterizedTypeReference<List<EjectedPilotInfo>>() {
                    });

            ejectionsFromServer = responseEntity.getBody();
            return ejectionsFromServer;
        } catch (RestClientException e) {
            System.err.println("Could not get ejections: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

//    @Override
//    public EjectedPilotInfo getEjectedPilotById(HttpServletRequest request, int id) {
//        List<EjectedPilotInfo> ejectionsFromServer;
//        ResponseEntity<List<EjectedPilotInfo>> responseEntity = restTemplate.exchange(
//                EJECTION_SERVER_URL + "/ejections?name=" + NAMESPACE, HttpMethod.GET,
//                null, new ParameterizedTypeReference<List<EjectedPilotInfo>>() {
//                });
//
//        ejectionsFromServer = responseEntity.getBody();
//        ejectionsFromServer.stream().filter(ejectedPilotInfo -> ejectedPilotInfo.getId() == id);
//        EjectedPilotInfo ejectionFromServer = ejectionsFromServer.get(0);
//        ejectionFromServer.rescuedBy = getClientIdFromCookies(request.getCookies());
//
//        dataBase.update(ejectionFromServer);
//
//        return ejectionFromServer;
//    }



    private void updateEjections() {
        try {
            List<EjectedPilotInfo> updatedEjections = shiftNorth();

            List<EjectedPilotInfo> previousEjections = dataBase.getAllOfType(EjectedPilotInfo.class);

            List<EjectedPilotInfo> addedEjections = ejectionsToAdd(updatedEjections, previousEjections);
            List<EjectedPilotInfo> removedEjections = ejectionsToRemove(updatedEjections, previousEjections);

            addedEjections.forEach(dataBase::create);
            removedEjections.stream().map(EjectedPilotInfo::getId).forEach(id -> dataBase.delete(id, EjectedPilotInfo.class));
        } catch (RestClientException e) {
            System.err.println("Could not get ejections: " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Nullable
    private List<EjectedPilotInfo> shiftNorth() {
        ResponseEntity<List<EjectedPilotInfo>> responseEntity = restTemplate.exchange(
                EJECTION_SERVER_URL + "/ejections?name=" + NAMESPACE, HttpMethod.GET,
                null, new ParameterizedTypeReference<List<EjectedPilotInfo>>() {
                });
        List<EjectedPilotInfo> ejectionsFromServer = responseEntity.getBody();

        if (ejectionsFromServer != null) {
            for (EjectedPilotInfo ejectedPilotInfo : ejectionsFromServer) {
                Coordinates c = ejectedPilotInfo.getCoordinates();
                ejectedPilotInfo.setCoordinates(new Coordinates(c.lat + SHIFT_NORTH, c.lon));
            }
        }
        return ejectionsFromServer;
    }

    private List<EjectedPilotInfo> ejectionsToRemove(List<EjectedPilotInfo> updatedEjections, List<EjectedPilotInfo> previousEjections) {
        return ejectionsToAdd(previousEjections, updatedEjections);
    }

    private List<EjectedPilotInfo> ejectionsToAdd(List<EjectedPilotInfo> updatedEjections, List<EjectedPilotInfo> previousEjections) {
        return listOperations.subtract(updatedEjections, previousEjections, new Entity.ByIdEqualizer<>());
    }

}
